/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClienteServidor;

import java.io.DataInputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java .util.logging.Logger;
import javax.swing.DefaultListModel;

/**
 *
 * @author champ
 */
public class TAHiloCliente extends Thread {
    
    private Socket SocketCliente;
    private DataInputStream entrada;
    private TACliente cliente;
    private ObjectInputStream entradaObjeto;
    
    public TAHiloCliente(Socket SocketCliente, TACliente cliente) { //Se crea en todo este proceso el ingreso de los clientes
        this.SocketCliente = SocketCliente;
        this.cliente = cliente;
        
    }
    
    public void run() {
        while(true) {
            try {
                entrada =  new DataInputStream(SocketCliente.getInputStream());
                cliente.mensajeria(entrada.readUTF());
                
                entradaObjeto = new ObjectInputStream(SocketCliente.getInputStream());
                cliente.actualizarLista((DefaultListModel) entradaObjeto.readObject());
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(TAHiloCliente.class.getName()).log(Level.SEVERE, null, ex);//Se obtiene los datos para hacer "login"
            } catch (IOException ex) {
                Logger.getLogger(TAHiloCliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
